package com.infobip.urlshorterer.urlshorterer.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.AccountRequestVM;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.AccountResponseVM;
import com.infobip.urlshorterer.urlshorterer.services.UtilsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccountsResourceTest {

    private MockMvc mockMvc;

    @MockBean
    private UtilsService utilsService;

    @Autowired
    private AccountsResource accountsResource;

    private ObjectMapper objectMapper;
    private AccountRequestVM accountRequestVM;
    private AccountResponseVM errorResponseVM;
    private Account account;

    @Before
    public void setUp() throws Exception {
        objectMapper = new ObjectMapper();

        accountRequestVM = new AccountRequestVM("testId");

        account = new Account(1l, accountRequestVM.getAccountId(), "password", true, null);

        errorResponseVM = new AccountResponseVM(false, "Account with accountId = " + accountRequestVM.getAccountId() + " already exists.", null);

        this.mockMvc = standaloneSetup(this.accountsResource).build();
    }

    @Test
    public void shouldCreateAccount() throws Exception {
        when(utilsService.checkForExistingAccount(accountRequestVM)).thenReturn(null);
        MvcResult result = mockMvc.perform(post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(accountRequestVM)))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        AccountResponseVM response = getObjectFromJson(result);
        assertNotNull(response);
        assertEquals(response.getDescription(), "Your account has been created.");
        assertTrue(response.isSuccess());
    }

    @Test
    public void shouldThrowAccountExists() throws Exception {
        when(utilsService.checkForExistingAccount(accountRequestVM)).thenReturn(errorResponseVM);

        MvcResult result = mockMvc.perform(post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(accountRequestVM)))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        AccountResponseVM response = getObjectFromJson(result);
        assertNotNull(response);
        assertEquals(response.getDescription(), errorResponseVM.getDescription());
        assertFalse(response.isSuccess());
        assertNull(response.getPassword());
    }

    private AccountResponseVM getObjectFromJson(MvcResult result) throws IOException {
        return objectMapper.readValue(result.getResponse().getContentAsString(), AccountResponseVM.class);
    }
}