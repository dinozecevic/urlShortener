package com.infobip.urlshorterer.urlshorterer.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infobip.urlshorterer.urlshorterer.enitities.ShortUrlVM;
import com.infobip.urlshorterer.urlshorterer.enitities.Url;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;
import com.infobip.urlshorterer.urlshorterer.repositories.UrlRepository;
import com.infobip.urlshorterer.urlshorterer.services.UtilsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UrlResourcesTest {

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @MockBean
    private UtilsService utilsService;

    @MockBean
    private UrlRepository urlRepository;

    @Autowired
    private UrlResources urlResources;

    private Account account = new Account(1l, "accountId", "password", true, null);


    private String BASE_URL = "http://localhost:8080/";
    private String shortUrl = "abcd";
    private Url url = new Url(1l, BASE_URL + "really-long", shortUrl, 302l, 0l, account.getId());

    @Before
    public void setUp() throws Exception {

        objectMapper = new ObjectMapper();

        when(urlRepository.findOneByUrl(url.getUrl())).thenReturn(url);
        when(utilsService.getAuthenticatedAccount()).thenReturn(account);
        when(urlRepository.save(any())).thenReturn(url);

        this.mockMvc = standaloneSetup(this.urlResources).build();

    }

    @Test
    public void shouldRegisterUrl() throws Exception {
        when(urlRepository.findOneByUrl(url.getUrl())).thenReturn(null);

        MvcResult result = mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(url)))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        ShortUrlVM response = getObjectFromJson(result);
        verify(urlRepository, times(1)).save(any());
        assertNotNull(response);
        assertEquals(response.getShortUrl(), url.getShortUrl());

    }

    private ShortUrlVM getObjectFromJson(MvcResult result) throws IOException {
        return objectMapper.readValue(result.getResponse().getContentAsString(), ShortUrlVM.class);
    }

}