package com.infobip.urlshorterer.urlshorterer.resources;

import com.infobip.urlshorterer.urlshorterer.enitities.Url;
import com.infobip.urlshorterer.urlshorterer.repositories.UrlRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RedirectResourceTest {

    private MockMvc mockMvc;

    @MockBean
    private UrlRepository urlRepository;

    @Autowired
    private RedirectResource redirectResource;

    private String BASE_URL = "http://localhost:8080/";
    private String shortUrl = "abcd";
    private Url url = new Url(1l, BASE_URL + "really-long", shortUrl, 302l, 0l, 1l);

    @Before
    public void setUp() throws Exception {

        when(urlRepository.findOneByShortUrl(BASE_URL + shortUrl)).thenReturn(url);

        this.mockMvc = standaloneSetup(this.redirectResource).build();

    }

    @Test
    public void shouldRedirectAndSaveVisitCount() throws Exception {
        mockMvc.perform(get("/" + shortUrl)
                .contentType(MediaType.APPLICATION_JSON));
        verify(urlRepository, times(1)).findOneByShortUrl(BASE_URL + shortUrl);
        verify(urlRepository, times(1)).save(any());
    }

}