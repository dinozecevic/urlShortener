package com.infobip.urlshorterer.urlshorterer.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infobip.urlshorterer.urlshorterer.enitities.Url;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;
import com.infobip.urlshorterer.urlshorterer.repositories.UrlRepository;
import com.infobip.urlshorterer.urlshorterer.services.UtilsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


@SpringBootTest
@RunWith(SpringRunner.class)
public class StatisticsResourceTest {


    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @MockBean
    private UrlRepository urlRepository;
    @MockBean
    private UtilsService utilsService;

    @Autowired
    private StatisticsResource statisticsResource;

    private String BASE_URL = "http://localhost:8080/";
    private String shortUrl = "abcd";

    private Account account = new Account(1l, "accountId", "password", true, null);
    private Url url = new Url(1l, BASE_URL + "really-long", shortUrl, 302l, 0l, 1l);

    @Before
    public void setUp() throws Exception {
        when(utilsService.getAuthenticatedAccount()).thenReturn(account);
        when(urlRepository.findAllForAccountId(account.getAccountId())).thenReturn(Arrays.asList(url));

        this.mockMvc = standaloneSetup(this.statisticsResource).build();

    }


    @Test
    public void shouldReturnStatistics() throws Exception {
        MvcResult result = mockMvc.perform(get("/statistic/" + account.getAccountId())
                .contentType(MediaType.APPLICATION_JSON)).andReturn();

        verify(urlRepository, times(1)).findAllForAccountId(account.getAccountId());
        verify(utilsService, times(1)).getAuthenticatedAccount();
    }


}