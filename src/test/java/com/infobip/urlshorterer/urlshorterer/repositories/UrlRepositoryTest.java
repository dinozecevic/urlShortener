package com.infobip.urlshorterer.urlshorterer.repositories;

import com.infobip.urlshorterer.urlshorterer.enitities.Url;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;


@SpringBootTest
@RunWith(SpringRunner.class)
@DataJpaTest
public class UrlRepositoryTest {

    @Autowired
    private UrlRepository urlRepository;

    @Autowired
    private AccountRepository accountRepository;


    private String BASE_URL = "http://localhost:8080/";
    private String shortUrl1 = "abcd1";
    private String shortUrl2 = "abcd2";
    private Account account1;
    private Account account2;

    private Url urlAccount1;
    private Url urlAccount2;

    @Before
    public void setUp() throws Exception {
        account1 = new Account(null, "account1", "password", true, null);
        account2 = new Account(null, "account2", "password", true, null);

        account1 = accountRepository.save(account1);
        account2 = accountRepository.save(account2);

        urlAccount1 = new Url(null, BASE_URL + "really-long1", shortUrl1, 302l, 0l, account1.getId());
        urlAccount1.setAccount(account1);
        urlAccount2 = new Url(null, BASE_URL + "really-long2", shortUrl2, 302l, 0l, account2.getId());
        urlAccount2.setAccount(account2);

        urlAccount1 = urlRepository.save(urlAccount1);
        urlAccount2 = urlRepository.save(urlAccount2);
    }

    @Test
    public void shouldFindOneByUrl() {
        Url response = urlRepository.findOneByUrl(urlAccount1.getUrl());
        assertNotNull(response);
        assertEquals(response, urlAccount1);
    }

    @Test
    public void shouldFindOneByShortUrl() {
        Url response = urlRepository.findOneByShortUrl(urlAccount1.getShortUrl());
        assertNotNull(response);
        assertEquals(response, urlAccount1);
    }

    @Test
    public void shouldFindAllForAccountId() {
        List<Url> responses = urlRepository.findAllForAccountId(account1.getAccountId());
        assertTrue(!responses.isEmpty());
        assertEquals(responses.size(), 1l);
        assertTrue(responses.contains(urlAccount1));
        assertFalse(responses.contains(urlAccount2));
    }

}