package com.infobip.urlshorterer.urlshorterer.repositories;

import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    private Account account;


    @Before
    public void setUp() throws Exception {
        account = new Account(null, "accountId", "password", true, null);

        account = accountRepository.save(account);

    }

    @Test
    public void shouldFindOneByAccountId() {
        Account response = accountRepository.findOneByAccountId("accountId");
        assertEquals(response, account);
    }

}