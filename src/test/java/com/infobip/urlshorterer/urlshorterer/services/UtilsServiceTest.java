package com.infobip.urlshorterer.urlshorterer.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.AccountRequestVM;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.AccountResponseVM;
import com.infobip.urlshorterer.urlshorterer.repositories.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UtilsServiceTest {

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UtilsService utilsService;

    private Account account = new Account(1l, "accountId", "password", true, null);
    private AccountRequestVM accountRequestVM;
    private AccountResponseVM errorResponseVM;


    @Before
    public void setUp() throws Exception {
        accountRequestVM = new AccountRequestVM("accountId");
        errorResponseVM = new AccountResponseVM(false, "Account with accountId = " + accountRequestVM.getAccountId() + " already exists.", null);

        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(account.getAccountId(), account.getPassword())
        );

        when(accountRepository.findOneByAccountId(any())).thenReturn(account);

        this.mockMvc = standaloneSetup(this.utilsService).build();

    }

    @Test
    public void shouldGetAuthenticatedAccount() {
        Account response = utilsService.getAuthenticatedAccount();

        verify(accountRepository, times(1)).findOneByAccountId(any());
        assertEquals(response, account);

    }

    @Test
    public void shouldCreateAccountAndReturnPassword() {
        String createdPassword = utilsService.createAccountAndReturnPassword(accountRequestVM);
        verify(accountRepository, times(1)).save(any());
        assertEquals(createdPassword.length(), 8);
    }

    @Test
    public void shouldCheckForExistingAccount() {
        when(accountRepository.findOneByAccountId(any())).thenReturn(null);

        AccountResponseVM response = utilsService.checkForExistingAccount(accountRequestVM);

        verify(accountRepository, times(1)).findOneByAccountId(accountRequestVM.getAccountId());
        assertNull(response);
    }

    @Test
    public void shouldReturnAccountExistsResponse() {
        AccountResponseVM response = utilsService.checkForExistingAccount(accountRequestVM);

        verify(accountRepository, times(1)).findOneByAccountId(accountRequestVM.getAccountId());

        assertNotNull(response);
        assertEquals(response, errorResponseVM);
    }

}