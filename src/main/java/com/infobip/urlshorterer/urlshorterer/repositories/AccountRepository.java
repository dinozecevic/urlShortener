package com.infobip.urlshorterer.urlshorterer.repositories;

import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("SELECT acc FROM Account acc WHERE acc.accountId = :accountId")
    Account findOneByAccountId(@Param("accountId") String accountId);

}
