package com.infobip.urlshorterer.urlshorterer.repositories;

import com.infobip.urlshorterer.urlshorterer.enitities.Url;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UrlRepository extends JpaRepository<Url, Long> {

    @Query("SELECT u FROM Url u WHERE u.url = :url")
    Url findOneByUrl(@Param("url") String url);

    @Query("SELECT u FROM Url u WHERE u.shortUrl = :shortUrl")
    Url findOneByShortUrl(@Param("shortUrl") String shortUrl);

    @Query("SELECT u FROM Url u JOIN FETCH u.account acc WHERE acc.accountId = :accountId")
    List<Url> findAllForAccountId(@Param("accountId") String accountId);

}
