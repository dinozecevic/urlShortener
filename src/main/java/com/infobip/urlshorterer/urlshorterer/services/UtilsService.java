package com.infobip.urlshorterer.urlshorterer.services;

import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.AccountRequestVM;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.AccountResponseVM;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Role;
import com.infobip.urlshorterer.urlshorterer.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.UUID;

@Service
public class UtilsService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Account getAuthenticatedAccount() {
        String accountId = SecurityContextHolder.getContext().getAuthentication().getName();
        return accountRepository.findOneByAccountId(accountId);
    }

    public String createAccountAndReturnPassword(AccountRequestVM accountRequest) {
        String password = UUID.randomUUID().toString().substring(0, 8);
        Account newAccount = new Account(null, accountRequest.getAccountId(), passwordEncoder.encode(password), true, Arrays.asList(new Role(null, "USER")));
        accountRepository.save(newAccount);
        return password;
    }

    public AccountResponseVM checkForExistingAccount(AccountRequestVM accountRequest) {
        Account existingAccount = accountRepository.findOneByAccountId(accountRequest.getAccountId());
        if (existingAccount != null) {
            return new AccountResponseVM(
                    false,
                    "Account with accountId = " + existingAccount.getAccountId() + " already exists.",
                    null);
        }
        return null;
    }

}
