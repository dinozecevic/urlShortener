package com.infobip.urlshorterer.urlshorterer.services;

import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;
import com.infobip.urlshorterer.urlshorterer.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Account account = accountRepository.findOneByAccountId(s);
        if (account == null) {
            throw new UsernameNotFoundException("Account doesn't exist.");
        }
        return new User(account.getAccountId(), account.getPassword(), AuthorityUtils.createAuthorityList("USER"));
    }
}
