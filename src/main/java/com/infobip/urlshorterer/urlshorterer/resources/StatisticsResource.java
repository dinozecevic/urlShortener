package com.infobip.urlshorterer.urlshorterer.resources;

import com.infobip.urlshorterer.urlshorterer.enitities.Url;
import com.infobip.urlshorterer.urlshorterer.repositories.UrlRepository;
import com.infobip.urlshorterer.urlshorterer.services.UtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/statistic")
public class StatisticsResource {

    @Autowired
    private UrlRepository urlRepository;

    @Autowired
    private UtilsService utilsService;

    @GetMapping("/{accountId}")
    public HashMap<String, Long> getUrlRedirectStatistics(@PathVariable("accountId") String accountId) {
        List<Url> urls = urlRepository.findAllForAccountId(
                utilsService.getAuthenticatedAccount().getAccountId()
        );
        return urls.stream().collect(Collectors.toMap(Url::getUrl, Url::getVisitCount, (a, b) -> b, HashMap::new));
    }

}
