package com.infobip.urlshorterer.urlshorterer.resources;

import com.infobip.urlshorterer.urlshorterer.enitities.ShortUrlVM;
import com.infobip.urlshorterer.urlshorterer.enitities.Url;
import com.infobip.urlshorterer.urlshorterer.exceptions.httpExceptions.BadRequestException;
import com.infobip.urlshorterer.urlshorterer.repositories.UrlRepository;
import com.infobip.urlshorterer.urlshorterer.services.UtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/register")
public class UrlResources {

    private final static String BASE_URL = "http://localhost:8080/";

    @Autowired
    private UrlRepository urlRepository;

    @Autowired
    private UtilsService utilsService;

    @PostMapping
    public ShortUrlVM registerUrl(@RequestBody Url url) {
        Url existingUrl = urlRepository.findOneByUrl(url.getUrl());
        if (existingUrl != null) {
            throw new BadRequestException("Provided url has already been shortened.");
        }
        url.setShortUrl(BASE_URL + UUID.randomUUID().toString().substring(0, 6));
        url.setAccount(utilsService.getAuthenticatedAccount());
        url = urlRepository.save(url);
        return new ShortUrlVM(url.getShortUrl());
    }
}
