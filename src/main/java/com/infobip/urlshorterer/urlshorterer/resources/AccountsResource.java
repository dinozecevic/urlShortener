package com.infobip.urlshorterer.urlshorterer.resources;

import com.infobip.urlshorterer.urlshorterer.enitities.accounts.AccountRequestVM;
import com.infobip.urlshorterer.urlshorterer.enitities.accounts.AccountResponseVM;
import com.infobip.urlshorterer.urlshorterer.exceptions.httpExceptions.BadRequestException;
import com.infobip.urlshorterer.urlshorterer.repositories.AccountRepository;
import com.infobip.urlshorterer.urlshorterer.services.UtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/accounts")
public class AccountsResource {

    @Autowired
    private UtilsService utilsService;

    @PostMapping
    public AccountResponseVM saveAccount(@RequestBody AccountRequestVM accountRequest) {
        AccountResponseVM existingAccount = utilsService.checkForExistingAccount(accountRequest);
        if (existingAccount != null) {
            return existingAccount;
        }
        String password = utilsService.createAccountAndReturnPassword(accountRequest);
        return new AccountResponseVM(true,
                "Your account has been created.",
                password);
    }


}