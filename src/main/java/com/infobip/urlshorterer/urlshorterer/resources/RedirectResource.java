package com.infobip.urlshorterer.urlshorterer.resources;

import com.infobip.urlshorterer.urlshorterer.enitities.Url;
import com.infobip.urlshorterer.urlshorterer.exceptions.httpExceptions.NotFoundException;
import com.infobip.urlshorterer.urlshorterer.repositories.UrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class RedirectResource {

    private final static String BASE_URL = "http://localhost:8080/";

    @Autowired
    private UrlRepository urlRepository;

    @GetMapping("/{shortUrl}")
    public void redirectAndSaveVisitCount(@PathVariable String shortUrl,
                                          HttpServletResponse response) throws IOException {
        Url existingUrl = urlRepository.findOneByShortUrl(BASE_URL + shortUrl);
        if (existingUrl == null) {
            throw new NotFoundException("Original url for " + shortUrl + "does not exist.");
        }
        existingUrl.setVisitCount(existingUrl.getVisitCount() + 1);
        urlRepository.save(existingUrl);

        redirectToUrl(response, existingUrl);
    }

    private void redirectToUrl(HttpServletResponse response, Url existingUrl) throws IOException {
        response.setStatus(existingUrl.getRedirectType().intValue());
        response.sendRedirect(existingUrl.getUrl());
    }

}
