package com.infobip.urlshorterer.urlshorterer.resources;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/help")
public class HelperResource {

    @GetMapping
    public String getHelpPage() {
        return "help";
    }
}
