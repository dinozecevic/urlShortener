package com.infobip.urlshorterer.urlshorterer.exceptions;

import java.util.Date;

public class ErrorDetails {
    private Long statusCode;
    private Date timestamp;
    private String message;
    private String details;

    public ErrorDetails(Long statusCode, Date timestamp, String message, String details) {
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
        this.statusCode = statusCode;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }

    public Long getStatusCode() {
        return statusCode;
    }
}
