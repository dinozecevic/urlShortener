package com.infobip.urlshorterer.urlshorterer.enitities;

public class ShortUrlVM {

    private String shortUrl;

    public ShortUrlVM() {
    }

    public ShortUrlVM(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShortUrlVM that = (ShortUrlVM) o;

        return shortUrl != null ? shortUrl.equals(that.shortUrl) : that.shortUrl == null;
    }

    @Override
    public int hashCode() {
        return shortUrl != null ? shortUrl.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ShortUrlVM{" +
                "shortUrl='" + shortUrl + '\'' +
                '}';
    }
}
