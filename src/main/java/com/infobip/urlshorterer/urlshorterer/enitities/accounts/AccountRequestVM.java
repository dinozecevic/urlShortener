package com.infobip.urlshorterer.urlshorterer.enitities.accounts;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class AccountRequestVM {

    @JsonProperty("AccountId")
    @NotNull
    private String accountId;

    public AccountRequestVM() {
    }

    public AccountRequestVM(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountRequestVM that = (AccountRequestVM) o;

        return accountId.equals(that.accountId);
    }

    @Override
    public int hashCode() {
        return accountId.hashCode();
    }

    @Override
    public String toString() {
        return "AccountRequestVM{" +
                "accountId='" + accountId + '\'' +
                '}';
    }
}
