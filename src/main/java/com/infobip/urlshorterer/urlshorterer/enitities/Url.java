package com.infobip.urlshorterer.urlshorterer.enitities;

import com.infobip.urlshorterer.urlshorterer.enitities.accounts.Account;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Url {

    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String url;
    private String shortUrl;
    private Long redirectType;
    private Long visitCount;

    @Column(name = "ACCOUNT_ID", insertable = false, updatable = false)
    private Long accountId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;


    public Url() {
    }

    public Url(Long id, @NotNull String url, String shortUrl, Long redirectType, Long visitCount, Long accountId) {
        this.id = id;
        this.url = url;
        this.shortUrl = shortUrl;
        this.redirectType = redirectType;
        this.visitCount = visitCount;
        this.accountId = accountId;
        this.account = account;
    }

    @PrePersist
    public void prePersist() {
        if(this.redirectType == null) {
            this.redirectType = 302l;
        }

        if(this.visitCount == null) {
            this.visitCount = 0l;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public Long getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(Long redirectType) {
        this.redirectType = redirectType;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public Long getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(Long visitCount) {
        this.visitCount = visitCount;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Url url1 = (Url) o;

        if (id != null ? !id.equals(url1.id) : url1.id != null) return false;
        if (!url.equals(url1.url)) return false;
        if (shortUrl != null ? !shortUrl.equals(url1.shortUrl) : url1.shortUrl != null) return false;
        if (redirectType != null ? !redirectType.equals(url1.redirectType) : url1.redirectType != null) return false;
        if (visitCount != null ? !visitCount.equals(url1.visitCount) : url1.visitCount != null) return false;
        return accountId != null ? accountId.equals(url1.accountId) : url1.accountId == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + url.hashCode();
        result = 31 * result + (shortUrl != null ? shortUrl.hashCode() : 0);
        result = 31 * result + (redirectType != null ? redirectType.hashCode() : 0);
        result = 31 * result + (visitCount != null ? visitCount.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Url{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", shortUrl='" + shortUrl + '\'' +
                ", redirectType=" + redirectType +
                ", visitCount=" + visitCount +
                ", accountId=" + accountId +
                '}';
    }
}
