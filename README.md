**HOW TO INSTALL**

After cloning project, navigate to project folder. To create executable jar file, run:

```
mvn clean install -DskipTests=true
java -jar target/urlshorterer-0.0.1-SNAPSHOT.jar
```

Or instead of above commands, run:
```
mvn spring-boot:run
```
After application has started, visit http://localhost:8080/help for more information on how to use app.